# run:
# 	rm -f p2
# 	gcc -DDISP -DPUFF -o p2 main.c
# 	./p2

run:
	rm -f p2
	clang -g -DDISP -DPUFF -o p2 main.c
	valgrind --tool=helgrind --read-var-info=yes ./p2

# WARNING: Having the -fsanitize=thread flag in my code stopped it from running concurrently
# (annoyingly)
# -g -fsanitize=thread -O3

# This tests for timeout / deadlock
# run:
# 	rm -f p2
# 	gcc -o p2 main.c
# 	@for i in $$(seq 1 1000); do \
# 		echo "Run $$i"; \
# 		gtimeout 10s ./p2; \
# 		if [ $$? -eq 124 ]; then \
# 			echo "Timeout detected on run $$i"; \
# 			exit 1; \
# 		fi; \
# 	done

# run:
# 	rm -f p2 log.txt full_log.txt
# 	gcc -DDISP -o p2 main.c
# 	@for i in $$(seq 1 10000); do \
# 		echo "Run $$i"; \
# 		OUTPUT=$$(gtimeout 10s ./p2); \
# 		EXIT_CODE=$$?; \
# 		echo "$$OUTPUT" >> full_log.txt; \
# 		echo "$$OUTPUT" | tail -n 1 >> log.txt; \
# 		if [ $$EXIT_CODE -eq 124 ]; then \
# 			echo "Timeout detected on run $$i" >> log.txt; \
# 			echo "Timeout detected on run $$i"; \
# 			exit 1; \
# 		fi; \
# 	done

# 	@echo "Creating analytics file..."
# 	@gawk 'BEGIN { FS = "[ ,=]+" } \
# 	{ \
# 		split($$0, parts, " = "); \
# 		n = split(parts[1], names, ","); \
# 		m = split(parts[2], values, " "); \
# 		for (i = 1; i <= n; i++) { \
# 			varName = names[i]; \
# 			varValue = values[i]; \
# 			count[varName][varValue]++; \
# 		} \
# 	} \
# 	END { \
# 		PROCINFO["sorted_in"] = "@ind_str_asc"; \
# 		first = 1; \
# 		for (var in count) { \Makefile:6: *** missing separator.  Stop.
# 			if (first) { \
# 				first = 0; \
# 			} else { \Makefile:6: *** missing separator.  Stop.
# 				printf "==================================================\n"; \
# 			} \
# 			printf "Variable %s:\n", var; \
# 			for (val in count[var]) { \
# 				printf "    - %s appears %d time(s)\n", val, count[var][val]; \
# 			} \
# 		} \
# 	}' log.txt > analytics.txtMakefile:6: *** missing separator.  Stop.