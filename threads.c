//Practical 2 Threads
//
// Task: Modify code below to ensure action-functions run atomically.
// 
//     Do not modify the order in which action-functions are called.
//     Do not add or modify action-function calls.

#include<pthread.h>

// Globals that don't need mutexes:
// gH is only accessed in thread 1 
// gN is only accessed in thread 5
// gQ is only accessed in thread 4
// gS is only accessed in thread 3
// gX is only accessed in thread 2
// gP is never accessed

// Globals that need mutexes:
// gA is shared between thread1, thread2, and thread5
// gI is shared between thread1, thread2, and thread5
// gJ is shared between thread2, thread3, and thread4
// gT is shared between thread3, thread4, and thread5
// gB is shared between thread1 and thread2
// gC is shared between thread3 and thread4
// gM is shared between all five threads
pthread_mutex_t gA_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gI_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gJ_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gT_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gB_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gC_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gM_mutex = PTHREAD_MUTEX_INITIALIZER;

// gD and gK are only ever used together (in thread1, thread2, thread3 and thread4)
// So one lock can be used for both
pthread_mutex_t gD_and_gK_mutex = PTHREAD_MUTEX_INITIALIZER;

// Helgrind was telling me that printf was causing a data race
// I then found out printf is not threadsafe, so I added a mutex for it
pthread_mutex_t printf_mutex = PTHREAD_MUTEX_INITIALIZER;

// Declare condition variables
pthread_cond_t gI_is_4 = PTHREAD_COND_INITIALIZER;
pthread_cond_t gI_is_7 = PTHREAD_COND_INITIALIZER;
pthread_cond_t gI_is_10 = PTHREAD_COND_INITIALIZER;

pthread_cond_t gC_is_less_than_1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t gC_is_greater_than_1 = PTHREAD_COND_INITIALIZER;

// Declare helper function
void signal_threads_waiting_for_gI();
void signal_threads_waiting_for_gC();

// We can add mutex locking and unlocking, and condition variable synchronisation here
void *Thread1(void *threadid) {
  pthread_mutex_lock(&printf_mutex);
  printf( "Thread1 Running\n" );
  pthread_mutex_unlock(&printf_mutex);

  pthread_mutex_lock(&gA_mutex);
  action11();   // accesses gA
  pthread_mutex_unlock(&gA_mutex);

  action12();   // accesses gH, but gH is only used in thread 1

  pthread_mutex_lock(&gA_mutex);
  action13();   // accesses gA
  pthread_mutex_unlock(&gA_mutex);

  pthread_mutex_lock(&gB_mutex);
  action14();   // accesses gB
  pthread_mutex_unlock(&gB_mutex);


  pthread_mutex_lock(&gD_and_gK_mutex);
  action15();   // accesses gK and gD
  pthread_mutex_unlock(&gD_and_gK_mutex);


  pthread_mutex_lock(&gM_mutex);
  action16();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gI_mutex);
  action17();   // accesses gI
  signal_threads_waiting_for_gI();
  
  DISPLAY("Waiting for %s\n","(gI == 4)");
  while (!((gI == 4))) {
    pthread_cond_wait(&gI_is_4, &gI_mutex);
  };

  puff();

  action18();   // accesses gI


  signal_threads_waiting_for_gI();
  pthread_mutex_unlock(&gI_mutex);


  pthread_exit(NULL);
}


void *Thread2(void *threadid) {
  pthread_mutex_lock(&printf_mutex);
  printf( "Thread2 Running\n" );
  pthread_mutex_unlock(&printf_mutex);

  pthread_mutex_lock(&gD_and_gK_mutex);
  action21();   // accesses gD and gK
  pthread_mutex_unlock(&gD_and_gK_mutex);

  pthread_mutex_lock(&gM_mutex);
  action22();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gJ_mutex);
  action23();   // accesses gJ
  pthread_mutex_unlock(&gJ_mutex);

  pthread_mutex_lock(&gA_mutex);
  action24();   // accesses gA
  pthread_mutex_unlock(&gA_mutex);

  pthread_mutex_lock(&gB_mutex);
  action25();   // accesses gB
  pthread_mutex_unlock(&gB_mutex);

  action26();   // accesses gX, but gX is only used in thread2

  pthread_mutex_lock(&gI_mutex);
  action27();   // accesses gI
  signal_threads_waiting_for_gI();



  DISPLAY("Waiting for %s\n","(gI == 7)");
  while (!((gI == 7))) {
    pthread_cond_wait(&gI_is_7, &gI_mutex);
  };
  puff();

  action28();   // accesses gI
  signal_threads_waiting_for_gI();
  pthread_mutex_unlock(&gI_mutex);


  pthread_exit(NULL);
}


void *Thread3(void *threadid) {
  pthread_mutex_lock(&printf_mutex);
  printf( "Thread3 Running\n" );
  pthread_mutex_unlock(&printf_mutex);

  pthread_mutex_lock(&gM_mutex);
  action31();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gT_mutex);
  action32();   // accesses gT
  pthread_mutex_unlock(&gT_mutex);

  action33();   // accesses gS, but gS is only used in thread3

  pthread_mutex_lock(&gD_and_gK_mutex);
  action34();   // accesses gK and gD
  pthread_mutex_unlock(&gD_and_gK_mutex);


  pthread_mutex_lock(&gJ_mutex);
  action35();   // accesses gJ
  pthread_mutex_unlock(&gJ_mutex);

  pthread_mutex_lock(&gM_mutex);
  action36();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gC_mutex);
  action37();   // accesses gC
  signal_threads_waiting_for_gC();



  DISPLAY("Waiting for %s\n","(gC < 1)");
  while (!((gC < 1))) {
    pthread_cond_wait(&gC_is_less_than_1, &gC_mutex);
  };
  puff();

  action38();   // accesses gC
  signal_threads_waiting_for_gC();
  pthread_mutex_unlock(&gC_mutex);


  pthread_exit(NULL);
}


void *Thread4(void *threadid) {
  pthread_mutex_lock(&printf_mutex);
  printf( "Thread4 Running\n" );
  pthread_mutex_unlock(&printf_mutex);

  pthread_mutex_lock(&gD_and_gK_mutex);
  action41();   // accesses gK and gD
  pthread_mutex_unlock(&gD_and_gK_mutex);

  action42();   // accesses gQ, but gQ is only used in thread4

  pthread_mutex_lock(&gJ_mutex);
  action43();   // accesses gJ
  pthread_mutex_unlock(&gJ_mutex);

  pthread_mutex_lock(&gT_mutex);
  action44();   // accesses gT
  pthread_mutex_unlock(&gT_mutex);

  pthread_mutex_lock(&gT_mutex);
  action45();   // accesses gT
  pthread_mutex_unlock(&gT_mutex);

  pthread_mutex_lock(&gM_mutex);
  action46();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gC_mutex);
  action47();   // accesses gC
  signal_threads_waiting_for_gC();

  DISPLAY("Waiting for %s\n","(gC > 1)");
  while (!((gC > 1))) {
    pthread_cond_wait(&gC_is_greater_than_1, &gC_mutex);
  };
  puff();

  action48();   // accesses gC
  signal_threads_waiting_for_gC();
  pthread_mutex_unlock(&gC_mutex);

  pthread_exit(NULL);
}


void *Thread5(void *threadid) {
  pthread_mutex_lock(&printf_mutex);
  printf( "Thread5 Running\n" );
  pthread_mutex_unlock(&printf_mutex);

  pthread_mutex_lock(&gT_mutex);
  action51();   // accesses gT
  pthread_mutex_unlock(&gT_mutex);

  pthread_mutex_lock(&gA_mutex);
  action52();   // accesses gA
  pthread_mutex_unlock(&gA_mutex);

  pthread_mutex_lock(&gA_mutex);
  action53();   // accesses gA
  pthread_mutex_unlock(&gA_mutex);

  action54();   // accesses gN, but gN is only used in thread5

  pthread_mutex_lock(&gM_mutex);
  action55();   // accesses gM
  pthread_mutex_unlock(&gM_mutex);

  pthread_mutex_lock(&gI_mutex);
  action56();   // accesses gI
  signal_threads_waiting_for_gI();


  DISPLAY("Waiting for %s\n","(gI == 10)");
  while (!((gI == 10))) {
    pthread_cond_wait(&gI_is_10, &gI_mutex);
  };
  puff();

  action57();   // accesses gI
  signal_threads_waiting_for_gI();
  pthread_mutex_unlock(&gI_mutex);

  pthread_exit(NULL);
}

// Checks all conditions for condition variables, then signals if neccessary 
// Only call while gI is locked
void signal_threads_waiting_for_gI(){
  if (gI == 4){
    pthread_cond_signal(&gI_is_4);
    return;
  }

  if (gI == 7){
    pthread_cond_signal(&gI_is_7);
    return;
  }

  if (gI == 10){
    pthread_cond_signal(&gI_is_10);
    return;
  }
}

// Checks all conditions for condition variables, then signals if neccessary 
// Only call while gC is locked
void signal_threads_waiting_for_gC(){
  if (gC < 1){
    pthread_cond_signal(&gC_is_less_than_1);
    return;
  }

  if (gC > 1){
    pthread_cond_signal(&gC_is_greater_than_1);
    return;
  }
}